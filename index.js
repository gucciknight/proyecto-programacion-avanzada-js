class Vehiculos{
  brand = '';
  color = '';
  type = '';
  model = '';
  displacement = 0;
  price = 0;

  constructor(brand, color, type, model, displacement, price) {
    this.brand = marca;
    this.color = color;
    this.type= tipo;
    this.model = modelo;
    this.displacement = displacement;
    this.price = price;
  }
}

class Node { // Vamos a indexar por patas
  nodeValues = [];     // Representa los animales que cumplen con las caracteristicas de patas
  // Si 6 animales tienen 4 patas, lo logico es que todos los animales de 4 patas queden dentro del arreglo valores
  nodeValue = '';
  left = null;
  right = null;

  constructor(nodeValue) {
    this.nodeValue = nodeValue;
  }

  indexer(vehiculo, value) {
    if (value === this.nodeValue) {
      this.nodeValues.push(vehiculo)
    } else if (value < this.nodeValue) {
      if (this.left) {
        this.left.indexer(vehiculo, value);
      } else {
        this.left = new Nodo(value);
        this.left.indexer(vehiculo, value);
      }
    } else if (value > this.nodeValue) {
      if (this.derecho) {
        this.right.indexer(vehiculo, value);
      } else {
        this.right = new Nodo(value);
        this.right.indexer(vehiculo, value);
      }
    }
  }

  search(value) {
    if (value === this.nodeValue) {
      return this.nodeValues;
    } else if (value < this.nodeValue) {
      if (this.izquierdo) {
        return this.izquierdo.busqueda(value);
      } else {
        // ESTO QUIERE DECIR QUE NO HAY RESULTADOS EN EL ARBOL CON LOS PARAMETROS QUE ESTAMOS BUSCANDO
        alert('No ha resultados madafdacka!');
        return [];
      }
      
    } else if (value > this.nodeValue) {
      if (this.derecho) {
        return this.derecho.busqueda(value);
      } else {
        // ESTO QUIERE DECIR QUE NO HAY RESULTADOS EN EL ARBOL CON LOS PARAMETROS QUE ESTAMOS BUSCANDO
        alert('No ha resultados madafdacka!');
        return [];
      }

    }
  }
}